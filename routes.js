const express = require('express');
const oracledb = require('oracledb');
const dbConfig = require('./dbConfig');
const app = express();

app.get('/consulta1', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 1
        const result = await connection.execute("SELECT index_name, column_name, table_name FROM user_ind_columns");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta2', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 2
        const result = await connection.execute("SELECT i.table_name, COUNT(i.table_name) FROM user_tables t INNER JOIN user_indexes i ON t.table_name = i.table_name GROUP BY i.table_name");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta3', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 3
        const result = await connection.execute("SELECT constraint_name, constraint_type, table_name FROM user_constraints WHERE constraint_type = 'P' ORDER BY table_name");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta4', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 4
        const result = await connection.execute("SELECT trigger_name, trigger_type, status, table_name FROM user_triggers");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta5', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 5
        const result = await connection.execute("SELECT table_name, column_name, SUM(data_length) FROM user_tab_columns GROUP BY table_name, column_name ORDER BY table_name");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta6', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 6
        const result = await connection.execute("SELECT table_name, SUM(data_length) FROM user_tab_columns GROUP BY table_name ORDER BY table_name");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta7', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 7
        const result = await connection.execute("SELECT table_name, blocks FROM user_tables");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta8', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 8
        const result = await connection.execute("SELECT object_name FROM user_procedures");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});

app.get('/consulta9', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 9
        const result = await connection.execute("SELECT SUM(bytes/1048576) FROM user_extents");
        if (result.rows.length === 0) {
            throw new Error("No data selected from table.");
        }

        return res.json(result.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});



app.get('/consulta10', async (req, res) => {
    let connection;
    try{
        //CONEXION CON ORACLE
        connection = await oracledb.getConnection(dbConfig);
    
        //CONSULTA 10
        const result = await connection.execute(
            "Select Distinct(blocks) from user_tables"
        );
        const result1 = await connection.execute(
            "SELECT table_name, TRUNC(SUM(data_length)/"+result.rows[0][0]+") FROM user_tab_columns GROUP BY table_name ORDER BY table_name"
        );

        return res.json(result1.rows);
    
    //ERRORES
    } catch(err) {
        console.log("Error: ", err);
    } finally {
        if (connection) {
            try {
            await connection.close();
            } catch(err) {
            console.log("Error when closing the database connection: ", err);
            }
        }
    }
    res.end();
});


//exports
module.exports = app;