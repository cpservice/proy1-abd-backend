//require
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

//initializations
const app = express();
app.set('port', 8000); //PORT FROM SERVER

//middlewares
app.use(cors()); //ACCESS CONTROL HTTP
app.use(bodyParser.urlencoded({extended: false})) //REQUEST IN BODY
app.use(bodyParser.json()) //JSON

//routes
app.use(require('./routes'));

//server
app.listen(app.get('port'), () => {
    console.log('Server listen on port', app.get('port'));
});